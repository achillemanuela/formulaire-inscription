<!DOCTYPE html>
<html>
<head>
<title>FORMULAIRE D'INSCRIPTION</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Enregistrement" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<link rel="stylesheet" type="text/css" href="https://www.sublimeacademie.com/assets/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media="all" href="https://www.sublimeacademie.com/assets/css/WELCOME/style.css" />
<script type="text/javascript" src="https://www.sublimeacademie.com/assets/javascript/js_style/jquery-1.11.0.min.js" > </script>
<script type="text/javascript" src="https://www.sublimeacademie.com/assets/javascript/js_style/bootstrap.js" > </script>
<script type="text/javascript" src="https://www.sublimeacademie.com/assets/bootstrap/js/bootstrap.min.js" > </script>

<link rel="stylesheet" type="text/css" media="all" href="https://www.sublimeacademie.com/assets/css/../fonts/material-icon/css/material-design-iconic-font.min.css" />
<!-- <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css"> -->
<!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> -->
<!-- <link rel="stylesheet" href="css/style.css"> -->



<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='//fonts.googleapis.com/css?family=Catamaran:400,100,300,500,700,600,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/png" href="https://www.sublimeacademie.com/assets/images/app/favicon.png" >
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},2000);
        });
    });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164775187-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-164775187-1');
	</script>
</head>
<body>
    <div class="header1" id="home">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                    <span class="icon-bar"> </span>
                </button>
                <h1><a class="navbar-brand" href="https://www.sublimeacademie.com/index.php/Welcome/index.sbl">Sublime<br /><span>Academie</span></a></h1>
                </div>
                

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right margin-top cl-effect-2">
                                                     <li ><a class="active"  href="https://www.sublimeacademie.com/index.php/Welcome/index.sbl">Accueil</a></li>
                            <li><a  href="https://www.sublimeacademie.com/index.php/Cour/index.sbl">Cours</a></li>
                        
                                                  <li><a  href="https://www.sublimeacademie.com/index.php/Forum/index.sbl">forum</a></li>
                         <li><a  href="https://www.sublimeacademie.com/index.php/Actualite/index.sbl">Actualité</a></li>
                        <li><a  href="https://www.sublimeacademie.com/index.php/Contact/index.sbl">Contact</a></li>    
                        <!-- <li><a  href="https://www.sublimeacademie.com/index.php/Welcome/partenaires.sbl">Nos partenaires</a></li>    -->
                        

                    </ul>
                    <div class="clearfix"> </div>
                </div>

                <div class="login-pop">
                      
                                                                     <div id="loginpop">
                            <form action="https://www.sublimeacademie.com/index.php/Welcome/connexion.sbl">
                                <button >Mon Compte</button>
                            </form>
                        </div>
                                        
                            
             
                   

                </div>
                <script type="text/javascript" src="https://www.sublimeacademie.com/assets/javascript/js_style/menu_jquery.js" > </script>            </div>
        </nav>

       
    </div> 


<link rel="stylesheet" type="text/css" media="all" href="https://www.sublimeacademie.com/assets/css/css_home/style2.css" />

<div class="content">
    <div class="banner">
           

          
        <div class="services" style="padding-bottom: 80px;">
            <div class="container">   
               
                <div class="row logo_title">
                    
                    <h1 align="center">
                        <img style=" height: 100px; width: 130px" src="https://www.sublimeacademie.com/assets/images/app/logo.png">
                       
                    </h1>
                </div>
                <div class="row">
                <div class="col-md-1"></div>
                <div class="  col-md-5 col-sm-6 col-xs-12 ">
                   <div class="signup-content">
                        <div class="signup-form">
                            <form class="register-form" id="login-form" action="https://www.sublimeacademie.com/index.php/Compte/connexion.sbl" method="post">
                                <h4 class="form-title">Connectez vous à votre compte </h4>
                                <div class="form-group">
                                    <label for="email"><i class="zmdi zmdi-email"></i></label>
                                    <input type="email" name="email" placeholder="Entrez votre Email" id="email" required=""> 
                                     
                                </div>
                                <div class="form-group">
                                    <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                    <input type="password" name="password" id="your_pass" placeholder="Password" required=""/>
                                    
                                </div>
                                <h6><a href="#">Mot de passe oublie ?</a> / <a href="#" id="pas_inscrire">Pas encore inscrire ?</a></h6>
                                <div class="form-group">
                                    
                                    <label for="agree-term" class="label-agree-term">Se souvenir de moi ? <input type="checkbox" /></label>
                                    
                                </div>
                                <div class="footer-connect">
                                    <input type="submit" id="signup" class="form-submit"  value="SE CONNECTER" >
                                                                    </div>
                            </form>
                           <!--  <div class="social-login">
                                <span class="social-label">Se connecter avec:</span>
                                <ul class="socials">
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                                    <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="signup-content" style="">
                        <div class="signup-form">
                            
                            
                            <form method="post" class="register-form" id="register-form" action="https://www.sublimeacademie.com/index.php/Compte/inscription.sbl" >
                                <h4 class="form-title">Creez votre compte</h4>
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                    <input type="text" name="nom" id="name" placeholder="Nom" required=""/>
                                </div>

                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                    <input type="text" name="prenom" id="name" placeholder="Prenom" required=""/>
                                </div>
                                <div class="form-group">
                                     <select name="sexe" class="sexe form-control" required="">
                                        <option value="Homme">Homme</option>
                                        <option value="Femme">Femme</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-time material-icons-name"></i></label>
                                    <input type="date" name="date_naissance" required="" >
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select name="codTel" class="code_pays form-control"></select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="number" min="650000000" style=" color: #00959F;" placeholder="Votre numéro ici!!!" name="tel_eleve" required="">
                                        </div>
                                        <input type="hidden" name="indice_pays" id="indice_pays" value="CM" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email"><i class="zmdi zmdi-email"></i></label>
                                    <input type="email" name="email" id="email" placeholder="Your Email" required=""/>
                                </div>
                                
                                <div class="form-group">
                                    <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                    <input type="password" name="password" id="pass" placeholder="Mot de passe" required=""/>
                                </div>
                                <div class="form-group">
                                    <select name="classe1" class="form-control"  required="">
                                                                                        <option value="1" >Terminale C</option>
                                                                                            <option value="2" >Terminale D</option>
                                                                                            <option value="3" >Terminale I</option>
                                                                                            <option value="4" >Terminale A</option>
                                                                                            <option value="5" >Première C</option>
                                                                                            <option value="6" >Première D</option>
                                                                                            <option value="7" >Première A</option>
                                                                                            <option value="8" >Première I</option>
                                                                                            <option value="9" >Troisième</option>
                                                                                            <option value="10" >Deutsche Prüfungen</option>
                                                                                </select>
                                        <!-- <div class="col-md-6 col-sm-6">
                                            <input type="radio" name="section" value="2" id="Anglophone" required=""><span>Anglophone</span>
                                        </div> -->
                                    <!-- <select name="classe2" id="classeAnglophone" class="form-control" required="" >
                                        <option selected="selected" disabled="true" >Choisir votre classe</option>
                                                                            </select> -->
                                </div>
                                <!-- <div class="form-group">
                                    <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                    <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                                </div> -->
                                <div class="footer-connect ">
                                    <input type="submit" name="signup" id="signup" class="form-submit" value="CREER LE COMPTE"/>
                                                                    </div>
                            </form>
                        </div>
                    </div>
            
                </div>
               
                </div>
            </div> 
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>

</div>
<div class="clearfix"> </div>