 <!DOCTYPE html>
<html>
<head>
    <title>FORMULAIRE D'INSCRIPTION</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Css/font-awesome.css">
    <link rel="stylesheet" href="../Css/ionicons.min.css">
    <script type="text/javascript" src="../Js/jquery-3.6.0.min.js" > </script>
    <style>
        @media screen and (max-width:768px){
          .img{  
             height: 80px; 
             width: 80px; 
             border-radius: 50%; 
             margin-left: 40px;
          }  
        }
    </style>
</head>   

<body>
    <div class="container-fluid" style="background-image: url(../Images/DSC_0026.JPG);background-size: cover;">   
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-12" style="background-color: #000;padding-bottom: 20px;margin-top: 40px;">
                <div class="signup-content" style="">
                    <div class="">
                        <form method="POST" class="" id="register-form" action="traitement_formulaire.php" >
                            <h2 class="form-title" style="text-align: center; color: #00BFFF;">INSCRIPTION</h2>
                            <div class="form-group">
                                <label for="name" style="color: #00BFFF"><i class="glyphicon glyphicon-user"></i>&nbsp NAME</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Your   Name" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="surname" style="color: #00BFFF"><i class="glyphicon glyphicon-user"></i>&nbsp &nbsp SURNAME</label>
                                <input type="text" class="form-control" name="surname" id="surname" placeholder="Your   Surname" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="sexe" style="color: #00BFFF"><i class="fa fa-transgender"></i>&nbsp &nbsp SEXE</label>
                                <select name="sexe" id="sexe" class="form-control" required="">
                                    <option value="" selected="selected" disabled=""> Your sexe</option>
                                    <option value="Homme">Man</option>
                                    <option value="Femme">Woman</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="birthday" style="color: #00BFFF"><i class="glyphicon glyphicon-calendar"></i>&nbsp &nbsp BIRTHDAY</label>
                                <input type="date" id="birthday" class="form-control" name="birthday" required="" >
                            </div>
                            <div class="form-group">
                                <label for="country" style="color: #00BFFF"><i class="glyphicon glyphicon-globe"></i>&nbsp &nbsp COUNTRY</label>
                                <select name="country" id="country" class="form-control" required="">
                                                <option value="" selected="selected" disabled="">Your   Country</option>
                                                <option value="Benin +229"> Benin <span>+229</span></option>
                                                <option value="Bolivia+591"> Bolivia <span>+591</span></option>
                                                <option value="Botswana +267"> Botswana <span>+267</span></option>
                                                <option value="Brazil +55"> Brazil <span>+55</span></option>
                                                <option value="Bulgaria+359"> Bulgaria <span>+359</span></option>
                                                <option value="Burkina Faso+226"> Burkina Faso <span>+226</span></option>
                                                <option value="Burundi +257"> Burundi <span>+257</span></option>
                                                <option value="Cambodia +855"> Cambodia <span>+855</span></option>
                                                <option value="Cameroon +237"> Cameroon <span>+237</span></option>
                                                <option value="Canada +1"> Canada <span>+1</span></option>
                                                <option value="Cape Verde +238"> Cape Verde <span>+238</span>
                                                <option value="Central African Republic +236"> Central African Republic <span>+236</span></option>
                                                <option value="Chad "> Chad <span>+235</span></option>
                                                <option value="Chile +56"> Chile <span>+56</span></option>
                                                <option value="China +86"> China <span>+86</span></option>
                                                <option value="Colombia +57"> Colombia <span>+57</span></option>
                                                <option value="Comoros and Mayotte +269"> Comoros and Mayotte <span>+269</span></option>
                                                <option value="Congo +242"> Congo <span>+242</span></option>
                                                <option value="Congo Dem Rep +243"> Congo Dem Rep <span>+243</span></option>
                                                <option value="Costa Rica +506"> Costa Rica <span>+506</span></option>
                                                <option value="Cote d'Ivoire +225"> Cote d'Ivoire <span>+225</span></option>
                                                <option value="Croatia +385"> Croatia <span>+385</span></option>
                                                <option value="Cuba +53"> Cuba <span>+53</span></option>
                                                
                                                <option value="Czech Republic +420"> Czech Republic <span>+420</span></option>
                                                <option value="Denmark +45"> Denmark <span>+45</span></option>
                                               
                                                <option value=" Djibouti +253"> Djibouti <span>+253</span></option>
                                                <option value="Ecuador +593"> Ecuador <span>+593</span></option>
                                                <option value="Egypt +20"> Egypt <span>+20</span></option>
                                                
                                                <option value=" Equatorial Guinea +240"> Equatorial Guinea <span>+240</span></option>
                                                <option value="Eritrea +291"> Eritrea <span>+291</span></option>
                                                
                                                <option value="Ethiopia +251"> Ethiopia <span>+251</span></option>
                                                
                                               
                                                <option value="Finland +358"> Finland <span>+358</span></option>
                                                <option value="France +33"> France <span>+33</span></option>
                                           
                                                <option value="Gabon +241"> Gabon <span>+241</span></option>
                                                <option value="Gambia +220"> Gambia <span>+220</span></option>
                                                
                                                <option value="Kuwait +965"> Kuwait<span>+965</span></option>
                                                
                                              
                                                <option value="Liberia +231"> Liberia<span>+231</span></option>
                                                <option value="Libya +218"> Libya<span>+218</span></option>
                                               
                                                <option value="Martinique +596"> Martinique <span>+596</span></option>
                                                <option value="Mauritania +222"> Mauritania <span>+222</span></option>
                                                <option value="Mexico +52"> Mexico <span>+52</span></option>
                                               
                                                <option value="Monaco +377"> Monaco <span>+377</span></option>
                                            
                                                <option value="Morocco +212"> Morocco <span>+212</span></option>
                                                <option value="Mozambique +258"> Mozambique <span>+258</span></option>
                                                <option value="Myanmar +95"> Myanmar <span>+95</span></option>
                                                <option value="Namibia +264"> Namibia <span>+264</span></option>
                                                
                                                <option value="Netherlands +31"> Netherlands <span>+31</span></option>
                                            
                                                <option value="New Zealand +64"> New Zealand <span>+64</span></option>
                                               
                                                <option value="Niger +227"> Niger <span>+227</span></option>
                                                <option value="Nigeria +234"> Nigeria <span>+234</span></option>
                                           
                                                <option value="Philippines +63"> Philippines <span>+63</span></option>
                                                <option value="Portugal +351"> Portugal <span>+351</span></option>
                                               
                                                <option value="Sao Tome and Principe +239"> Sao Tome and Principe<span>+239</span></option>
                                                <option value="Saudi Arabia +966"> Saudi Arabia<span>+966</span></option>
                                                <option value="Senegal +221"> Senegal<span>+221</span></option>
                                                <option value="Serbia +381"> Serbia<span>+381</span></option>
                                            
                                                <option value="Sierra Leone +232"> Sierra Leone<span>+232</span></option>
                                    
                                                
                                                <option value="Somalia +252"> Somalia<span>+252</span></option>
                                                <option value="South Africa +27"> South Africa<span>+27</span></option>
                                                <option value="South Sudan +211"> South Sudan<span>+211</span></option>
                                                <option value="Spain +34"> Spain<span>+34</span></option>
                                            
                                                <option value="Sudan +249"> Sudan<span>+249</span></option>
                                              
                                                <option value="Swaziland +268"> Swaziland<span>+268</span></option>
                                                <option value="Sweden +46"> Sweden<span>+46</span></option>
                                                <option value="Switzerland +41"> Switzerland<span>+41</span></option>
                                                <option value="Trinidad and Tobago +1868"> Trinidad and Tobago<span>+1868</span></option>
                                                <option value="Tunisia +216"> Tunisia<span>+216</span></option>
                                                <option value="Turkey +90"> Turkey<span>+90</span></option>
                                                
                                                <option value=" Uganda +256"> Uganda<span>+256</span></option>
                                                <option value="Ukraine +380"> Ukraine<span>+380</span></option>
                                                <option value="United Arab Emirates +971"> United Arab Emirates<span>+971</span></option>
                                                <option value="United Kingdom +44"> United Kingdom<span>+44</span></option>
                                                <option value="Venezuela +58"> Venezuela<span>+58</span></option>
                                                <option value="Vietnam +84"> Vietnam<span>+84</span></option>
                                              
                                              
                                                <option value="Yemen +967"> Yemen<span>+967</span></option>
                                                <option value="Zambia +260"> Zambia<span>+260</span></option>
                                                <option value="Zimbabwe +263"> Zimbabwe<span>+263</span></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="number" style="color: #00BFFF"><i class="glyphicon glyphicon-phone"></i>&nbsp &nbsp NUMBER</label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" id="code" class="form-control" style=" color: #00959F;" placeholder="Code" name="code">
                                    </div>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" min="660000000" style=" color: #00959F;" placeholder="Your   Number" name="num" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" style="color: #00BFFF"><i class="glyphicon glyphicon-envelope"></i>&nbsp &nbsp EMAIL</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your   Email" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="pass" style="color: #00BFFF"><i class="glyphicon glyphicon-lock"></i>&nbsp &nbsp PASSWORD</label>
                                <input type="password" class="form-control" name="password" id="pass" placeholder="Your   Password" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="photo" style="color: #00BFFF"> <i class="glyphicon glyphicon-picture"></i>&nbsp &nbsp PROFILE PICTURE</label>                       
                                <input type="file" name="image" id="image" class="form-control" accept="image/*" onchange="loadFile(event)" required="">                       
                                <div class="img col-md-offset-5 col-md-7" style="margin-top: 20px; margin-bottom: 20px; height: 120px; width: 120px; border-radius: 50%;">          
                                    <img id="pp" class="img" style="height: 120px; width: 120px; border-radius: 50%; border: 1px solid rgba(240, 255, 255);" />                       
                                </div> 
                            </div>
                            <div class="footer-connect col-md-4">
                                <input type="submit" name="sign_up" id="sign_up" class="form-submit form-control ac" style=" color: #000000; font-weight: bold; font-size: 18px; background-color: #00BFFF; margin-top: 170px; margin-left: -170px;" value="VALIDER"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">     
        var loadFile = function(event) {        
        var profil = document.getElementById('pp');         
        profil.src = URL.createObjectURL(event.target.files[0]);       };   
    </script> 

    <script type="text/javascript">
        var name=document.getElementById('name');
        var surname=document.getElementById('surname');
        // var sexe=document.getElementById('sexe');
        // var birthday=document.getElementsByTagName('input')[2];
        // var country=document.getElementById('country');
        var num=document.getElementsByTagName('input')[3];
        var email=document.getElementById('email');
        var password=document.getElementsByTagName('input')[5];
        var code=document.getElementById('code');
        var sign_up=document.getElementById('sign_up');
        var formulaire=document.getElementById('register-form');


        name.addEventListener('blur', testname);
        function testname(){
            if(name.value.length<2){
                name.innerHTML = "Le nom doit avoir au minimum 2 lettres";
                name.style.opacity='1'; 
                return false;
            }
            else if (contient_specialchart(name.value)){
                name.innerHTML = "Le nom ne doit pas avoir des caracteres speciaux tels que @ ou % ...";
                name.style.opacity = '1';
                return false;
            } 
            else{
            return true;
            }

        }

        function contient_specialchart(char){
            var special = ['@','!','~','<','>','?',' " '," ' ",'*','(',')','^','%','$','#','&','{','}','[',']',';',];

            for (var i = special.length - 1; i >= 0; i--) { //pour chaque element de special
                for (var id in char) {  // on se rassure qu'il ne soit pas dans char
                    if(char[id]==special[i]){
                        return true;
                    }
                }
            }
            return false;
        }


        surname.addEventListener('blur', testsurname);
        // console.log(prenom);
        function testsurname(){
            if(surname.value.length<4){
                surname.innerHTML = "Le prénom doit avoir au minimum 2 lettres";
                surname.style.opacity='1'; 
                return false;
            }
             else if (contient_specialchart(surname.value)){
                surname.innerHTML = "Le nom ne doit pas avoir des caracteres speciaux tels que @ ou % ...";
                surname.style.opacity = '1';
                return false;
            }
            else{
                return true;
            }
        }
 

        country.addEventListener('blur', testcode);
        function testcode(){
            var codetest=country.value.split('+');
             code.innerHTML=codetest[1];
        }

        email.addEventListener('blur', testmail);
        function testmail(){
            var email_indic='0';
            var email_format=[],email_ending=[];
            email_format=['gmail.com','outlook.com','yahoo.com','yahoo.fr','hotmail.com'];
            var email_ok;

            for(var i=0; i<email.value.length; i++){
                if(email.value[i]=='@'){
                    email_indic=email.value[i];
                }
            }
            
            if(email_indic=='@'){
                email_ending=email.value.split('@');
                
                for (var i = 0; i < email_format.length; i++) {
                    
                    if(email_format[i]==email_ending[1]){
                        email_ok=true;
                        email.style.backgroundColor='rgb(232,240,254)';
                        break;
                    }
                    else{
                        email.innerHTML = "Le format de mail attendu est 'exemple@xmail.xxx";
                        email.style.opacity='1';
                        email_ok=false;
                    }
                }
            }
            else{
                email_ok=false;
            }
            
        }
         
    </script>
</body>