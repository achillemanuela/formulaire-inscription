<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css%22%3E%22%3E">
   <link rel=" stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css%22%3E%22%3E">
   <link rel=" stylesheet" href="../bootstrap/css/bootstrap.min.css">

   <title>Formulaire</title>

   <style>
      @media screen and (max-width:768px){
      }
      @media screen and (min-width:768px) and (max-width:992px){
      }  

</style>
</head>

<body>

   <!-- connect to Database -->

   <?php
   $bdd = new PDO('mysql:host=localhost;dbname=traitement_formulaire', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

   // Insertion des donnees en BD

   $dataInsert = $bdd->prepare('INSERT into formulaire (name,surname,sexe,birthday,country,num,email,password,image) VALUES(?,?,?,?,?,?,?,?,?)');

   $dataInsert->execute(array(
      $_POST['name'],
      $_POST['surname'],
      $_POST['sexe'],
      $_POST['birthday'],
      $_POST['country'],
      $_POST['num'],
      $_POST['email'],
      $_POST['password'],
      $_POST['image'],
   ));


   $response = $bdd->query('SELECT * FROM formulaire');
   echo '<div class="container-fluid">
      <div class="row">
      <div class="table-responsive-md table-responsive-sm table-responsive-xl">
      <table class="table table-striped table-sm table-xl">
      <div class="col-md-12 col-sm-12 col-xl-12">
      <thead class="bg-primary">
      <td>ID</td>
      <td class="name">NAME</td>
      <td class="surname">SURNAME</td>
      <td class="sexe">SEXE</td>
      <td class="birthday">BIRTHDAY</td>
      <td class="country">COUNTRY</td>
      <td class="num">PHONE</td>
      <td class="email">EMAIL</td>
      
      <td class="image">IMAGE</td></thead></div>';
?>
<?php
      if(isset($_POST)){                       
      if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0){                 
         if ($_FILES['image']['size'] <= 3000000){                   
            $infosfichier = pathinfo($_FILES['image']['name']);                   
            $extension_upload = $infosfichier['extension'];                   
            $extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');                   
            if (in_array($extension_upload, $extensions_autorisees)){                     
               move_uploaded_file($_FILES['image']['tmp_name'], '../Images/' . basename($_FILES['image']['name']));        
               // echo "Image recu 100 % ! "."<br> <br> ";                     
               $p = $_FILES['image']['name'];                     
               // echo "<img style='height:100px; width:100px;' src='Images/".$p." ' >"."<br>";            
               $Image="<img style='height:80px; width:80px;' src='../Images/".$p." ' >"."<br>";                     
               // echo  $Image;

            }
               else {
                  echo 'extention non-autorisee';
               }
         }
            else {
               echo 'Image trop Volumineuse';
            }
      }
   }

   while ($donnees = $response->fetch()) {
      echo '<div class="col-md-12 col-sm-12 col-xl-12">
         <tbody class="">
         <td class="bg-primary">' . $donnees['id'] . '</td>
         <td>' . $donnees['name'] . '</td>
         <td>' . $donnees['surname'] . '</td>
         <td>' . $donnees['sexe'] . '</td>
         <td>' . $donnees['birthday'] . '</td>
         <td>' . $donnees['country'] . '</td>
         <td>' . $donnees['num'] . '</td>
         <td>' . $donnees['email'] . '</td>
         
         <td><img src="../Images/'.$donnees['image'].'"; style=" width: 80px; height: 80px; border-radius:50%"></td>
      </tbody>';
   }

   echo '</div></table></div></div></div>';

?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="../bootstrap-4/js/bootstrap.min.js"></script> -->
<!-- <script src="../bootstrap-5/js/bootstrap.min.js"></script> -->

</body>

</html>